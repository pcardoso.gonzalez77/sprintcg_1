<script>
        function calcularVenc() {
            // Capturamos la fecha inicial que llega por el input
            let fechaInicialElemento = document.getElementById("fecha_ini");
            // Creamos la fechaInicial del tipo Date
            let fechaInicial = new Date(fechaInicialElemento.value);
            // Hasta que le sumemos los meses que indica el usuario son la misma fecha.
            let fechaFinal = fechaInicial;
            // Capturamos el valor en meses.
            var meses = document.getElementById("meses_contrato").value;
           // Se los sumamos a la variable mes
            let mes = fechaInicial.getMonth() * 1.0 + 1 + meses * 1.0;
            // Si la variable mes es mayor de 11 quiere decir que hemos saltado de año.
            while(mes > 11){
                // Al saltar de año le debemos sumar un año y restar 12 meses
                let dia = fechaInicial.getDate();
                let year = fechaInicial.getFullYear() *1.0 + 1;
                mes = mes - 12;
                fechaFinal= new Date(year,mes,dia);
            }
            // Creamos la cadena de texto fecha final
            var fechaFin = fechaFinal.getDate() + "/" + mes + "/" + fechaFinal.getFullYear();
            // Capturamos el elemento que la va a mostrar en el formulario
            fecha_finElemento = document.getElementById("fecha_fin");
            // Le asginamos la fecha de fin al elemento span que muestra la fecha final en el formulario
            fecha_finElemento.innerHTML = fechaFin;
            alert("La fecha fin de contrato es " + fechaFin);
        }
    </script>