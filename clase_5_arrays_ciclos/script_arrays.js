/*var auto1 = "Pickup Chevrolet 1928";
console.log(auto1);
var auto2 = "Coupe Torino 1975";
console.log(auto2);
var auto3 = "Mercury 47";
console.log(auto3);
var auto4 = "Renault Gordini 1969";
console.log(auto4);*/
//Agregando elementos
var autos = ["Pickup Chevrolet 1928", "Coupe Torino 1975", "Mercury 47", "Renault Gordini 1969"];
//ver elementos
console.log("---ver elementos---");
console.log(autos[0]);
console.log(autos[1]);
console.log(autos[2]);
console.log(autos[3]);
//Usando la palabra reservada new Array
var autos2 = new Array("Pickup Chevrolet 1928", "Coupe Torino 1975", "Mercury 47", "Renault Gordini 1969");
console.log("---Usando la palabra reservada new Array---");
console.log(autos2[0]);
console.log(autos2[1]);
console.log(autos2[2]);
console.log(autos2[3]);
//averiguar la cantidad de elementos
console.log("---cantidad de elementos arrays autos[]---");
console.log(autos.length);
//Agrega el elemento al principio
autos.unshift("elementoNuevoInicio"); 
console.log("---Agrega el elemento al principio---");
// Agrega elemento al final
autos.push("elementoNuevoFinal");
console.log("---Agrega elemento al final---");
//averiguar la cantidad de elementos
console.log("---cantidad de elementos arrays autos[] ---");
console.log(autos.length);
//Buscamos el elemento
var indice = autos.indexOf("Coupe Torino 1975");
//Obtenemos el elemento buscado
var unAuto = autos[indice];
console.log(indice);
//Eliminamos el primer elemento
autos.shift();
//Eliminamos el último elemento
autos.pop();
//Eliminamos un elemento en particular
//indice debe llevar el valor donde esta ubicado el elemento a eliminar
autos.splice(indice, 1);
//averiguar la cantidad de elementos
console.log("---cantidad de elementos arrays autos[] ---");
console.log(autos.length);
console.log("---Recorriendo colecciones con for ---");
for (var indice = 0; indice < autos.length; indice++) {
//Imprime uno a uno todos los autos
console.log(autos[indice]);
}
console.log("---Recorriendo colecciones con while ---");
var indice = 0;
while (indice < autos.length) {
        //Imprime uno a uno todos los autos
        // Estamos haciendo lo mismo que con el for
        console.log(autos[indice]);
        indice++;
}

